# Question asking platform
### Config
#### _Database_
| KEY | VALUE |
| ---- | ---- |
| HOST | db |
| USER | root |
| PASSWORD | root |
| NAME | qap |

### Commands
```sh  
[ ] php artisan migrate:fresh
[ ] php artisan db:seed
[ ] mysql -h db -u root -proot qap
```

### Rest Api's
#### _HTTP Header_
```sh 
[Content-Type: application/json]
[X-Requested-With: XMLHttpRequest]
```    

#### _Services_

| KEY | VALUE |
| ---- | ---- |
| EVENT | Insert question |
| URI | /api/v1/question |
| METHOD | POST |

##### Input:

```json

Poll - question_type.id = 1 

{"content": "Supposedly home to a “monster,” Loch Ness is one of many inland seas—or “lochs”—in which country?",
    "question_type_id": "1", 
    "answers": [ {
            "content": "Ireland"
        },
        {
            "content": "England"
        },
        {
            "content": "Wales"
        },
        {
            "content": "Scotland"
        }]
} 
```

```json

Trivia - Trivia: question_type.id =  2

{"content": "In which country are the world’s 10 coldest cities located?"
    "question_type_id": "2", 
    "answers": [ 
        {
            "content": "Canada",
            "is_correct": "0"
        },
        {
            "content": "Sweden",
            "is_correct": "0"
        },
        {
            "content": "United States",
            "is_correct": "0"
        },
        {
            "content": "Russia",
            "is_correct": "1"
        }]
} 
```

##### Output:

```json
{"question": {"id": {id} }}
``` 

| KEY | VALUE |
| ---- | ---- |
| EVENT | Get question |
| URI | /api/v1/question/%id% |
| METHOD | GET |

##### Output:

```json
{"question": {
    "id": 1,
    "uuid": "1c135b0e-72ad-4695-81aa-d7e6c3fe5ac8",
    "content": "Supposedly home to a “monster,” Loch Ness is one of many inland seas—or “lochs”—in which country?",
    "question_type_id": 1,
    "created_at": {
        "date": "2022-01-01 12:26:10.000000",
        "timezone_type": 3,
        "timezone": "UTC"
    },
    "updated_at": {
        "date": "2022-01-01 12:26:10.000000",
        "timezone_type": 3,
        "timezone": "UTC"
    },
    "deleted_at": null,
    "answers": [
        {
            "id": 1,
            "question_id": 1,
            "uuid": "42eed298-b218-425f-8cfc-6d593653b1d4",
            "content": "Ireland",
            "is_correct": 0,
            "votes_counter": 0,
            "created_at": "2022-01-01 12:26:10",
            "updated_at": "2022-01-01 12:26:10",
            "deleted_at": null
        },
        {
            "id": 2,
            "question_id": 1,
            "uuid": "c744a6d2-3bf1-4bf2-bc26-e555c08fd83e",
            "content": "England",
            "is_correct": 0,
            "votes_counter": 0,
            "created_at": "2022-01-01 12:26:10",
            "updated_at": "2022-01-01 12:26:10",
            "deleted_at": null
        },
        {
            "id": 3,
            "question_id": 1,
            "uuid": "6f559c02-6f90-454b-a2aa-e07a61977269",
            "content": "Wales",
            "is_correct": 0,
            "votes_counter": 0,
            "created_at": "2022-01-01 12:26:10",
            "updated_at": "2022-01-01 12:26:10",
            "deleted_at": null
        },
        {
            "id": 4,
            "question_id": 1,
            "uuid": "3a2f90aa-f0fc-4bbe-9d29-ae38ff05ff51",
            "content": "Scotland",
            "is_correct": 0,
            "votes_counter": 0,
            "created_at": "2022-01-01 12:26:10",
            "updated_at": "2022-01-01 12:26:10",
            "deleted_at": null
        }
    ]}
}|
```

| KEY | VALUE |
| ---- | ---- |
| EVENT | Add Vote |
| URI | /api/v1/question/%id%/answer/%answer_id% |
| METHOD | PUT |

##### Output:

```json
{"answer": {"votes_counter": {%votes_counter%} }} 
```