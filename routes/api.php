<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::group(['prefix' => '/v1/question-type'], function() {

    // get list of Question types
    Route::get('','QuestionTypeController@index');

    // get specific Question type
    Route::get('/{id}','QuestionTypeController@show');
});

Route::group(['prefix' => '/v1/question'], function() {

    // get list of Question
    Route::get('','QuestionController@index');

    // get specific Question
    Route::get('/{id}','QuestionController@show');

    // create new Question
    Route::post('','QuestionController@store');

    // create new Question
    Route::put('/{id}/answer/{answer_id}','QuestionController@updateVote');

    // delete a Question
    Route::delete('/{id}','QuestionController@destroy');
});
