<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Question;
use App\QuestionType;
use App\Answer;
use App\Http\Resources\QuestionResource;
use App\Http\Resources\AnswerResource;
use App\Http\Resources\QuestionCollection;

use Illuminate\Support\Str;

class QuestionController extends Controller
{
    public function index() {
        // Get all Question 
        $questions = Question::all()->load('answers');

        // Return a collection of $questions 
        return new QuestionCollection($questions);
    }

    public function show($id) {
        
        try{
            // get the Question
            $question = Question::findOrFail($id)->load('answers');

            // Return a single $question_type
            return new QuestionResource($question);
        } catch(\Exception $e) {
            return response()->json(['message'=>'Question not found!'], 404);
        }
    }
    public function store(Request $request) {

        $this->validate($request, [
            'content' => 'required|min:5',
            'question_type_id' => 'required|integer|exists:question_types,id',
            'answers' => 'required'
            ]
        );

        $question = new Question([
            'uuid' => Str::uuid()->toString(),
            'content' => $request->input('content'),
            'question_type_id' => $request->input('question_type_id'),
        ]);

        $question_type = QuestionType::find($request->input('question_type_id'));
        $answer_rows = $request->input('answers');
        $is_correct_counter = 0;

        foreach ( $answer_rows as $answer_row ) {
            $answer = new Answer([
                'uuid' => Str::uuid()->toString(),
                'content' => $answer_row['content'],
            ]);

            if ( $question_type->text == 'Trivia' ) {
                if ( isset($answer_row['is_correct']) && (int) $answer_row['is_correct'] == 1 ) {
                    if ( $is_correct_counter == 0 ) {
                        $is_correct_counter++; 
                    } else {
                        return response()->json(['message'=>'More then one correct answer for a trivia question!'], 406);  
                    }
                    $answer['is_correct'] = (int) $answer_row['is_correct'];   
                }
            }

            $answers[] = $answer;
        }

        if ( $question_type->text == 'Trivia' && $is_correct_counter == 0 ) {
            return response()->json(['message'=>'Trivia questions should contain one correct answer!'], 406);
        }

        try {
            $question->save();
            $question->answers()->saveMany($answers);
            
            $question->makeHidden(['uuid', 'phone_number']);
            return new QuestionResource($question);
        } catch (\Exception $e) {
            return response()->json(['message'=>'Database error while saving Question record!\n',
                                    'errorInfo' => $e->getMessage()], 502);
        }
    }

    public function updateVote($id, $answer_id ) {
        $answer = Question::findOrFail($id)->answers()->where('id',$answer_id )->first();
        $answer->votes_counter++;
        $answer->save();

        return new AnswerResource($answer);
    }

    public function destroy($id) {
        
        try{
            // get the Question
            $question = Question::findOrFail($id);
            
            if( $question->answers()->delete() ) {
                return new QuestionResource($question);
            } 
            
        } catch(\Exception $e) {
            return response()->json(['message'=>'Question not found!'], 404);
        }
    }
}
