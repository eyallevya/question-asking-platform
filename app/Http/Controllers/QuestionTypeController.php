<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\QuestionType;
use App\Http\Resources\QuestionTypeResource;

class QuestionTypeController extends Controller
{
    public function index() {
        // Get all Question types 
        $question_types = QuestionType::all();
        
        // Return a collection of $question_types 
        return QuestionTypeResource::collection($question_types);
    }

    public function show($id) {
        
        try{
            // get the Question type 
            $question_type = QuestionType::findOrFail($id);

            // Return a single $question_type
            return new QuestionTypeResource($question_type);
        } catch(\Exception $e) {
            return response()->json(['message'=>'Question type not found!'], 404);
        }
    }
}
