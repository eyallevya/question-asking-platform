<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class AnswerResource extends JsonResource
{
            /**
     * The "data" wrapper that should be applied.
     *
     * @var string
     */
    public static $wrap = 'answer';
    
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            // 'id' => $this->id,
            // 'question_id' =>  $this->question_id,
            // 'uuid' =>  $this->uuid,
            // 'content' =>  $this->content,
            // 'is_correct' =>  $this->is_correct,
            'votes_counter' => $this->votes_counter,
            // 'created_at' =>  $this->created_at,
            // 'updated_at' =>  $this->updated_at,
            // 'deleted_at' =>  $this->deleted_at,     
        ];
    }
}
