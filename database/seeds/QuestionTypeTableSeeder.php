<?php

use Illuminate\Database\Seeder;
use \App\QuestionType;

class QuestionTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $question_type = new QuestionType();
        $question_type->text = 'Poll';
        $question_type->save();

        $question_type = new QuestionType();
        $question_type->text = 'Trivia';
        $question_type->save();
    }
}
