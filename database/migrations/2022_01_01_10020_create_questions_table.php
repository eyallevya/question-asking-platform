<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuestionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('questions', function (Blueprint $table) {
            $table->increments('id');
            $table->uuid('uuid')->nullable(false);
            $table->string('content')->nullable(false);
            $table->unsignedInteger('question_type_id')->nullable(false);
            $table->timestamps();
            $table->softDeletes(); // deleted_at
            $table->foreign('question_type_id')->references('id')->on('question_types'); 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('questions');
    }
}
